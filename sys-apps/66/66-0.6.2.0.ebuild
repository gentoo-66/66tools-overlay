# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Small tools built around s6 and s6-rc programs"
HOMEPAGE="https://framagit.org/Obarun/66/"
SRC_URI="https://framagit.org/Obarun/66/-/archive/v${PV}/66-v${PV}.tar.bz2 -> 66-${PV}.tar.bz2"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
IUSE="doc doc-html man shared static static-libs static-pic"

REQUIRED_USE="static? ( static-libs )"

RDEPEND=">=dev-libs/skalibs-2.10.0.1:=[static-libs?]
        >=dev-lang/execline-2.7.0.1:=[static-libs?]
        >=sys-apps/s6-2.10.0.1:=[static-libs?]
        >=sys-apps/s6-rc-0.5.2.1:=[static-libs?]
        >=dev-libs/oblibs-0.1.2.0:=[static-libs?]
        acct-user/s6log
        acct-group/s6log"

DEPEND="${RDEPEND}
        ( >=app-text/lowdown-0.7.9 )"

S="${WORKDIR}/${PN}-v${PV}"

pkg_setup() {
    use doc-html && HTML_DOCS=( doc/html/. )
}

src_prepare() {
    default

    ## Avoid QA warning for LDFLAGS addition; avoid overriding -fstack-protector
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' -e '/-fno-stack-protector$/d' \
    configure || die

    ## Avoid unexpected path for html documentation, let einstalldocs do the job
    sed -i '/^install:/ s/install-html //' Makefile

    ## 66-boot option for Gentoo
    sed -i 's/66-boot/66-boot -m \/run/' skel/init 
}

src_configure() {
    econf \
        --bindir=/bin \
        --dynlibdir=/usr/$(get_libdir) \
        --libdir=/usr/$(get_libdir)/${PN} \
        --with-dynlib=/usr/$(get_libdir) \
        --with-lib=/usr/$(get_libdir)/s6 \
        --with-lib=/usr/$(get_libdir)/s6-rc \
        --with-lib=/usr/$(get_libdir)/oblibs \
        --with-lib=/usr/$(get_libdir)/skalibs \
        --with-lib=/usr/$(get_libdir)/execline \
        --with-s6-log-user=s6log \
        --with-s6-log-timestamp=iso \
        --with-system-module=/usr/share/66/module \
        --with-system-script=/usr/share/66/script \
        --with-system-service=/usr/share/66/service \
        --with-sysadmin-module=/etc/66/module \
        --with-sysadmin-service=/etc/66/service \
        --with-sysadmin-service-conf=/etc/66/conf \
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps \
        $(use_enable shared) \
        $(use_enable static allstatic) \
        $(use_enable static static-libc) \
        $(use_enable static-libs static)
}

src_compile() {
    if use doc-html ; then
            pushd "${WORKDIR}/${PN}-v${PV}" > /dev/null || die
                ./doc/make-html.sh || die
            popd > /dev/null   || die
    fi

    if use man ; then
            pushd "${WORKDIR}/${PN}-v${PV}" > /dev/null || die
                ./doc/make-man.sh  || die
            popd > /dev/null   || die
    fi
}

src_install() {
    emake DESTDIR="${D}" install

    use doc-html && einstalldocs

    if ! use doc && use doc-html ; then
        find "${ED}/usr/share/doc" -iname "AUTHOR*" | xargs rm
        find "${ED}/usr/share/doc" -iname "README*" | xargs rm
    fi

    if use doc && ! use doc-html ; then
        dodoc AUTHOR* README*
    fi

    use man && doman doc/man/man*/*.[158]

    mkdir "${D}/sbin"
    mkdir "${D}/usr/bin"

    mv "${D}/etc/66/init"       "${D}/sbin/"
    mv "${D}/etc/66/halt"       "${D}/usr/bin/"
    mv "${D}/etc/66/poweroff"   "${D}/usr/bin/"
    mv "${D}/etc/66/reboot"     "${D}/usr/bin/"
    mv "${D}/etc/66/shutdown"   "${D}/usr/bin/"
}

pkg_postinst() {

    elog "WARNING:"
    elog "Starting from version 0.5.1.0"
    elog "There is no more need to run manually 66-update"
    elog ""
    elog ""
    elog "WARNING:"
    elog "Starting from version 0.6.0.1"
    elog "You must reboot the computer after an upgrade"
    elog "Any change on the trees will be done after that"
    elog ""
    elog ""
    elog "WARNING:"
    elog "Starting from version 0.6.0.1"
    elog "CONFIG_PROTECT on /sbin/init is no longer necessary"
    elog "You can remove it from the make.conf file"
    elog ""
    elog ""
    elog "WARNING:"
    elog "Upgrading from version <0.6.0.1"
    elog "Due to a typo on older version affecting the build process"
    elog "1) Remove the 'boot' tree"
    elog "# 66-tree -R boot"
    elog "2) Create the 'boot' tree"
    elog "# 66-tree -n boot"
    elog "3) Enable the 'boot' tree"
    elog "# 66-enable -t boot -F boot@system"
    elog "4) Reboot"
    elog ""
    elog ""
    elog "WARNING:"
    elog "Upgrading from version <0.6.0.1"
    elog "Due to a change on 66-scandir and 66-scanctl interface"
    elog "You must run"
    elog "# 66-enable -t <tree> -F boot-user@<username>"
    elog "Replace <tree> by the name of your tree"
    elog "Replace <username> by the name of your user"
    elog ""
    elog ""
}
