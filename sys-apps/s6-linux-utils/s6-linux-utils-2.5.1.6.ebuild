# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Set of tiny linux utilities"
HOMEPAGE="https://www.skarnet.org/software/s6-linux-utils/"
SRC_URI="https://www.skarnet.org/software/${PN}/${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc shared static static-libs static-pic"

REQUIRED_USE="static? ( static-libs )"

RDEPEND=">=dev-libs/skalibs-2.10.0.1:=[static-libs?]"

DEPEND="${RDEPEND}"

pkg_setup() {

    use doc && HTML_DOCS=( doc/. )
}

src_prepare() {
    default

    # Avoid QA warning for LDFLAGS addition; avoid overriding -fstack-protector
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' -e '/-fno-stack-protector$/d' \
        configure || die
}

src_configure() {
    econf \
        --bindir=/bin \
        --datadir=/etc \
        --dynlibdir=/usr/$(get_libdir) \
        --libdir=/usr/$(get_libdir)/${PN} \
        --with-dynlib=/usr/$(get_libdir) \
        --with-lib=/usr/$(get_libdir)/skalibs \
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps \
        $(use_enable shared) \
        $(use_enable static allstatic) \
        $(use_enable static static-libc) \
        $(use_enable static-libs static) \
        $(use_enable static-pic all-pic)
}
