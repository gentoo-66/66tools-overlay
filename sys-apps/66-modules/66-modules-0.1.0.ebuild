# Copyright 2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

SRC_URI="https://git.obarun.org/obdev/modules/-/archive/v${PV}/${PN/66-}-v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~arm ~arm64"

DESCRIPTION="Utility script written in pure-POSIX sh to load modules from files"
HOMEPAGE="https://git.obarun.org/obdev/modules"

LICENSE="ISC"
SLOT="0"

RDEPEND="sys-apps/kmod"

S="${WORKDIR}/${PN/66-}-v${PV}"

src_configure() {
	./configure --bindir=/bin
}

src_install() {
	emake DESTDIR="${ED}" install
}
