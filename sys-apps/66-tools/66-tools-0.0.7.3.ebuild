# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Small tools and helpers for service scripts execution"
HOMEPAGE="https://framagit.org/Obarun/66-tools/"

SRC_URI="https://framagit.org/Obarun/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2 -> ${PN}-${PV}.tar.bz2"
LICENSE="ISC"

SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
IUSE="doc doc-html man shared static static-libs static-pic"

REQUIRED_USE="static? ( static-libs )"

RDEPEND=">=dev-libs/skalibs-2.10.0.1:=[static-libs?]
        >=dev-lang/execline-2.7.0.1:=[static-libs?]
        >=dev-libs/oblibs-0.1.2.0:=[static-libs?]"

DEPEND="${RDEPEND}
    ( >=app-text/lowdown-0.7.9 )"

S="${WORKDIR}/${PN}-v${PV}"

pkg_setup() {
    use doc-html && HTML_DOCS=( doc/html/. )
}

src_prepare() {
    default

    ## Avoid QA warning for LDFLAGS addition; avoid overriding -fstack-protector
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' -e '/-fno-stack-protector$/d' \
    configure || die

    ## Avoid unexpected path for html documentation, let einstalldocs do the job
    sed -i '/^install:/ s/install-html //' Makefile
}

src_configure() {
    econf \
        --bindir=/bin \
        --dynlibdir=/usr/$(get_libdir) \
        --libdir=/usr/$(get_libdir)/${PN} \
        --with-dynlib=/usr/$(get_libdir) \
        --with-lib=/usr/$(get_libdir)/oblibs \
        --with-lib=/usr/$(get_libdir)/skalibs \
        --with-lib=/usr/$(get_libdir)/execline \
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps \
        --with-ns-rule=/usr/share/66/script/ns \
        $(use_enable shared) \
        $(use_enable static allstatic) \
        $(use_enable static static-libc) \
        $(use_enable static-libs static) \
        $(use_enable static-pic all-pic)
}

src_install() {
    emake DESTDIR="${D}" install install-ns-rule

    use doc-html && einstalldocs

    if ! use doc && use doc-html ; then
        find "${ED}/usr/share/doc" -iname "AUTHOR*" | xargs rm
        find "${ED}/usr/share/doc" -iname "README*" | xargs rm
    fi

    if use doc && ! use doc-html ; then
        dodoc AUTHOR* README*
    fi

    use man && doman doc/man/man*/*.1
}
