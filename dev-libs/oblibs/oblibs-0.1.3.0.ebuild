# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="66 C library"
HOMEPAGE="https://framagit.org/Obarun/oblibs/"
SRC_URI="https://framagit.org/Obarun/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2 -> ${PN}-${PV}.tar.bz2"
LICENSE="ISC"
SLOT="0/$(ver_cut 2-)"
KEYWORDS="~amd64 ~arm ~x86"
IUSE="doc shared static static-libs static-pic"

REQUIRED_USE="static? ( static-libs )"

RDEPEND=">=dev-libs/skalibs-2.10.0.1:=[static-libs?]
        >=dev-lang/execline-2.7.0.1:=[static-libs?]"

DEPEND="${RDEPEND}"

S="${WORKDIR}/${PN}-v${PV}"

src_prepare() {
    default

    # Avoid QA warning for LDFLAGS addition; avoid overriding -fstack-protector
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' -e '/-fno-stack-protector$/d' \
        configure || die
}

src_configure() {
    econf \
        --bindir=/bin \
        --dynlibdir=/usr/$(get_libdir) \
        --libdir=/usr/$(get_libdir)/${PN} \
        --with-dynlib=/usr/$(get_libdir) \
        --with-lib=/usr/$(get_libdir)/execline \
        --with-lib=/usr/$(get_libdir)/skalibs \
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps \
        $(use_enable shared) \
        $(use_enable static allstatic) \
        $(use_enable static-libs static) \
        $(use_enable static-pic all-pic)
}

src_install() {
    emake DESTDIR="${D}" install
    use doc && dodoc AUTHOR* README*
}
