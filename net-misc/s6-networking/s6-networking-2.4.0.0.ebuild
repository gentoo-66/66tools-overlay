# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Suite of small networking utilities for Unix systems"
HOMEPAGE="https://www.skarnet.org/software/s6-networking/"
SRC_URI="https://www.skarnet.org/software/${PN}/${P}.tar.gz"

LICENSE="ISC"
SLOT="0/$(ver_cut 1-2)"
KEYWORDS="~amd64 ~x86"
IUSE="doc libressl +ssl shared static static-libs static-pic"

REQUIRED_USE="static? ( static-libs )"

RDEPEND=">=dev-libs/skalibs-2.10.0.1:=[static-libs?]
    >=dev-lang/execline-2.7.0.1:=[static-libs?]
    >=sys-apps/s6-2.10.0.1:=[static-libs?]
    !static? (
        >=net-dns/s6-dns-2.3.5.0:=
        ssl? (
            !libressl? ( dev-libs/openssl:0= )
            libressl? ( dev-libs/libressl:0= )
        )
    )"

DEPEND="${RDEPEND}
    >=net-dns/s6-dns-2.3.5.0[static-libs?]
    ssl? (
        !libressl? ( dev-libs/openssl )
        libressl? ( dev-libs/libressl[static-libs?] )
    )"

pkg_setup() {

    use doc && HTML_DOCS=( doc/. )

    if use libressl; then
        ssl_opts=( --enable-ssl=libressl)
    fi
}

src_prepare() {
    default

    # Avoid QA warning for LDFLAGS addition; avoid overriding -fstack-protector
    sed -i -e 's/.*-Wl,--hash-style=both$/:/' -e '/-fno-stack-protector$/d' \
        configure || die
}

src_configure() {
    econf \
        --bindir=/bin \
        --datadir=/etc \
        --dynlibdir=/usr/$(get_libdir) \
        --libdir=/usr/$(get_libdir)/${PN} \
        --with-dynlib=/usr/$(get_libdir) \
        --with-lib=/usr/$(get_libdir)/s6 \
        --with-lib=/usr/$(get_libdir)/s6-dns \
        --with-lib=/usr/$(get_libdir)/skalibs \
        --with-sysdeps=/usr/$(get_libdir)/skalibs/sysdeps \
        "${ssl_opts[@]}" \
        $(use_enable shared) \
        $(use_enable static allstatic) \
        $(use_enable static static-libc) \
        $(use_enable static-libs static) \
        $(use_enable static-pic all-pic)
}
