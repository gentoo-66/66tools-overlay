# Copyright 2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Virtual to select between different udev daemon providers"
SLOT="0"
KEYWORDS="amd64 arm arm64"

RDEPEND=">=sys-fs/eudev-2.1.1"
