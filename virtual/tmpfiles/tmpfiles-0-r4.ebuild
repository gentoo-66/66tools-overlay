# Copyright 2020-2021 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Virtual to select between different tmpfiles.d handlers"
SLOT="0"
KEYWORDS="amd64"

RDEPEND="sys-apps/66-opentmpfiles"
