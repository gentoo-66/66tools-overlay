# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Service to create a nested user scandir"
HOMEPAGE="https://framagit.org/pkg/obmods/scandir-66serv"
SRC_URI="https://framagit.org/pkg/obmods/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
IUSE="doc"

RDEPEND=">=sys-apps/66-0.6.0.1
        >=sys-apps/66-tools-0.0.7.1"

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
    econf \
        --bindir=/bin \
        --with-system-module=/usr/share/66/module \
        --with-system-service=/usr/share/66/service
}

src_compile() {
    emake DESTDIR="${D}"
}

src_install() {
    emake DESTDIR="${D}" install
    use doc && dodoc AUTHOR*
}
