# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Set of service to create a nested user scandir with console tracker and display manager support"
HOMEPAGE="https://framagit.org/pkg/obmods/boot-66serv"
SRC_URI="https://framagit.org/pkg/obmods/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
IUSE="consolekit doc elogind"

RDEPEND=">=sys-apps/66-0.5.1.0
        >=sys-apps/66-tools-0.0.6.1
        consolekit? ( 66-services/consolekit-66serv )
        elogind? ( 66-services/elogind-66serv )
        66-services/dbus-66serv
        66-services/console-tracker-66serv
        66-services/display-manager-66serv
        66-services/scandir-66serv"

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
    econf \
        --bindir=/bin \
        --with-system-module=/usr/share/66/module \
        --with-system-service=/usr/share/66/service
}

src_compile() {
    emake DESTDIR="${D}"
}

src_install() {
    emake DESTDIR="${D}" install
    use doc && dodoc AUTHOR*
}
