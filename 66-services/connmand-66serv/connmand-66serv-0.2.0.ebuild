# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="connmand service for 66 tools"
HOMEPAGE="https://framagit.org/pkg/observice/connmand-66serv"
SRC_URI="https://framagit.org/pkg/observice/${PN}/-/archive/master/${PN}-master.tar.gz?path=version/${PV}-1 -> ${PN}-${PV}.tar.gz"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
SERVICE="${PN/-66serv/}"

RDEPEND=">=sys-apps/66-0.3.0.3
        net-misc/connman"

S="${WORKDIR}/${PN}-master-version-${PV}-1"

src_prepare() {
    eapply_user
    sed "s:@VERSION@:${PV}:" -i version/${PV}-1/${SERVICE}
}

src_install() {
    dodir /usr/share/66/service
    insinto /usr/share/66/service
    doins version/${PV}-1/${SERVICE}
}
