# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="metalog service for 66 tools"
HOMEPAGE="https://framagit.org/pkg/observice/metalog-66serv"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
MY_SERVICE="metalog"

RDEPEND=">=sys-apps/66-0.3.0.3
        >=sys-apps/66-tools-0.0.5.1
        app-admin/metalog"

src_unpack() {
    mkdir "${S}" || die
}

src_prepare() {
    eapply_user

	for i in ${MY_SERVICE[@]} ; do
        sed -e "s:@VERSION@:${PV}:" "${FILESDIR}"/${i}.66 > ${i} || die
    done
}

src_install() {
    dodir /usr/share/66/service
    insinto /usr/share/66/service

    for i in ${MY_SERVICE[@]} ; do
        newins ${i} ${i} || die
    done
}
