# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Complete and portable set of services to properly boot a machine with 66 tools"
HOMEPAGE="https://framagit.org/pkg/obmods/boot-66serv"
SRC_URI="https://framagit.org/pkg/obmods/boot-66serv/-/archive/v${PV}/${PN}-v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
IUSE="btrfs doc doc-html dmraid iptables lvm man zfs"

RDEPEND=">=sys-apps/66-0.6.0.1
        >=sys-apps/66-tools-0.0.7.1
        >=sys-apps/s6-linux-utils-2.5.1.4
        >=sys-apps/s6-portable-utils-2.2.3.1
        >=app-shells/bash-4.4_p23-r1
        >=sys-apps/iproute2-5.1.0
        >=sys-apps/kmod-26-r1
        >=sys-apps/util-linux-2.33.2
        btrfs? ( sys-fs/btrfs-progs )
        dmraid? ( sys-fs/dmraid )
        iptables? ( net-firewall/iptables )
        lvm? ( sys-fs/lvm2 )
        zfs? ( sys-fs/zfs )"

S="${WORKDIR}/${PN}-v${PV}"

pkg_setup() {

    use doc-html && HTML_DOCS=( doc/html/. )
}

src_configure() {
    econf \
        --bindir=/bin \
        --libdir=/usr/$(get_libdir)/66 \
        --with-system-module=/usr/share/66/module \
        --with-system-script=/usr/share/66/script \
        --with-system-service=/usr/share/66/service
}

src_compile() {
    emake DESTDIR="${D}"
}

src_install() {
    emake DESTDIR="${D}" install

    find "${ED}/usr/share" -type d -name "doc" | xargs rm -r
    find "${ED}/usr/share" -type d -name "man" | xargs rm -r

    use doc && dodoc AUTHOR*
    use man && doman doc/man/man*/*.1

    if use doc-html ; then
        docinto html
        dodoc doc/html/boot@.html
    fi
}
