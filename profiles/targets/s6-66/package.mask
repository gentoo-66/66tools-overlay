# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

# Prevent others 'init' systems from being installed by masking the packages

sys-fs/udev
sys-apps/openrc
sys-apps/systemd
sys-apps/systemd-tmpfiles
sys-apps/systemd-utils
sys-apps/sysvinit
sys-process/runit
sys-process/daemontools

# Prevent update from gentoo repository

>dev-libs/skalibs-2.11.2.0
>dev-lang/execline-2.8.3.0
>sys-apps/s6-2.11.1.0
>sys-apps/s6-rc-0.5.3.1
>sys-apps/s6-linux-utils-2.5.1.7
>sys-apps/s6-portable-utils-2.2.4.0
>net-dns/s6-dns-2.3.5.4
>net-misc/s6-networking-2.5.1.1

>sys-auth/elogind-246.10-r2
>sys-kernel/dracut-056-r1
>virtual/service-manager-1
>virtual/udev-217-r6
>virtual/libudev-232-r7
>virtual/tmpfiles-0-r4
