# 66tools-overlay

An overlay bringing you the power of 66tools and s6

## Installing

Add and enable the overlay.

```bash
emerge -av dev-vcs/git eselect-repository
eselect repository add 66tools-overlay git https://framagit.org/architekt/66tools-overlay.git
eselect repository enable 66tools-overlay
emerge --sync 66tools-overlay
```

66tools-overlay bundles profiles which will build 66tools with the right USE flags. As they are marked experimental, you'll have to set them using the `--force` flag.

```bash
eselect profile list
eselect profile set --force {num}

# unmask packages
mkdir /etc/portage/package.accept_keywords
ln -s /var/db/repos/66tools-overlay/profiles/package.accept_keywords/s6-66tools.accept /etc/portage/package.accept_keywords/s6-66tools.accept

# rebuild
emerge -DNuav @world
emerge -a --depclean
```

Once 66tools is installed, **do not** reboot yet.

Create the boot tree.

```bash
66-tree -n boot
66-enable -t boot -F boot@system

# Optional: edit variables
66-env -t boot -e nano boot@system
66-enable -t boot -F boot@system
```

```bash
emerge -av dhcpcd-66serv sshd-66serv
66-tree -nEc system
66-enable dhcpcd sshd

66-intree -gz
```

```bash
nano /etc/66/init.conf
VERBOSITY=4
```

From there follow the official Gentoo install to finish the installation/configuration of your system in the chroot env.
...

You can now reboot safely.
